use bevy::{
    prelude::*, 
    render::{
        camera::{
            Camera,
            WindowOrigin
        },
        pass::ClearColor,
    }
};
use std::path::Path;

const WINDOW_WIDTH: f32 = 640.0;
const WINDOW_HEIGHT: f32 = 360.0;
const SCALE: f32 = 0.5;
const CHARACTER_LIST: [&str; 13] = [
    "groof",
    "bird_d",
    "jubby",
    "quil",
    "queue",
    "volc",
    "james",
    "shivers",
    "snacky",
    "crashies",
    "eli",
    "nesuki",
    "parf",
];

mod collider;
mod background;
mod gamestate;
mod groof;
mod pipe;
mod title_cards;

use background::*;
use collider::*;
use groof::*;
use gamestate::GameState;
use pipe::*;
use title_cards::*;

pub struct Scoreboard {
    score: usize,
}

#[derive(Default)]
struct Game {
    character_selection: usize,
}

fn setup_cameras(mut commands: Commands) {
    //Add a cameras to the world
    let mut ortho_camera = OrthographicCameraBundle::new_2d();
    ortho_camera.orthographic_projection.scale = SCALE;
    ortho_camera.orthographic_projection.window_origin = WindowOrigin::BottomLeft;
    

    commands.spawn_bundle(ortho_camera);
    commands.spawn_bundle(UiCameraBundle::default());
}

fn start_setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,

    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let character = CHARACTER_LIST[0];
    let file_path
        = Path::new("sprites")
            .join("title_cards")
            .join(format!("title_screen_{}.png", character)
    );
    let texture_handle: Handle<Texture> = asset_server.load(file_path);

    //Spawn Title card
    commands.spawn()
        .insert_bundle(SpriteBundle {
            material: materials.add(texture_handle.into()),
            transform: Transform {
                translation: Vec3::new(
                    (WINDOW_WIDTH / 2.0) * SCALE, 
                    (WINDOW_HEIGHT / 2.0) * SCALE, 
                    0.0
                ),
                scale: Vec3::new(SCALE, SCALE, SCALE),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Title);
}

fn play_setup(
    asset_server: Res<AssetServer>, 
    mut commands: Commands,
    game: Res<Game>,
    mut scoreboard: ResMut<Scoreboard>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    scoreboard.score = 0;

    let character = CHARACTER_LIST[game.character_selection];
    let file_path
        = Path::new("sprites")
            .join("flappers")
            .join(format!("{}.png", character)
    );
    let texture_handle: Handle<Texture> = asset_server.load(file_path);

    //Spawn groof
    commands.spawn()
        .insert_bundle(SpriteBundle {
            material: materials.add(texture_handle.into()),
            transform: Transform {
                translation: Vec3::new(
                    (WINDOW_WIDTH / 2.0) * SCALE, 
                    (WINDOW_HEIGHT / 2.0) * SCALE, 
                    2.0
                ),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Groof { 
            gravity: 550.0,
            max_speed: 200.0,
            velocity: Vec3::new(0.0, 0.0, 0.0)
        })
        .insert(CollisionBox {
            collider: Collider::Groof,
            offset: Vec3::new(0.0, 0.0, 0.0),
            size: Vec2::new(10.0, 10.0),
        });

    // Scoreboard text
    commands.spawn_bundle(TextBundle {
        text: Text {
            sections: vec![
                TextSection {
                    value: "Score: 0".to_string(),
                    style: TextStyle {
                        font: asset_server.load(Path::new("fonts").join("slkscr.ttf")),
                        font_size: 40.0,
                        color: Color::rgb(0.0, 0.0, 0.0),
                    },
                },
                TextSection {
                    value: "".to_string(),
                    style: TextStyle {
                        font: asset_server.load(Path::new("fonts").join("slkscr.ttf")),
                        font_size: 40.0,
                        color: Color::rgb(0.0, 0.0, 0.0),
                    },
                },
            ],
            ..Default::default()
        },
        style: Style {
            position_type: PositionType::Absolute,
            position: Rect {
                top: Val::Px(5.0),
                left: Val::Px(5.0),
                ..Default::default()
            },
            ..Default::default()
        },
        ..Default::default()
    });
}

fn display_start_message(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                margin: Rect { 
                    left: Val::Px(WINDOW_WIDTH / 3.0), 
                    right: Val::Px(0.0),
                    top: Val::Px(0.0),
                    bottom: Val::Px(WINDOW_HEIGHT - 40.0),
                },
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            material: materials.add(Color::NONE.into()),
            ..Default::default()
        })
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    format!("Press <R> to start!"),
                    TextStyle {
                        font: asset_server.load(Path::new("fonts").join("slkscr.ttf")),
                        font_size: 20.0,
                        color: Color::rgb(0.0, 0.0, 0.0),
                    },
                    Default::default(),
                ),
                ..Default::default()
            });
        });
}

fn pick_character (
    asset_server: Res<AssetServer>,
    mut commands: Commands,
    mut game: ResMut<Game>,
    mut materials: ResMut<Assets<ColorMaterial>>,
    title_query: Query<Entity, With<Title>>,
    keyboard_input: Res<Input<KeyCode>>
) {
    let mut direction = 0;
    if keyboard_input.just_pressed(KeyCode::A) || keyboard_input.just_pressed(KeyCode::Left) {
        direction -= 1;
    }
    if keyboard_input.just_pressed(KeyCode::D) || keyboard_input.just_pressed(KeyCode::Right) {
        direction += 1;
    }

    if direction != 0 {
        game.character_selection = (CHARACTER_LIST.len() + game.character_selection + direction).rem_euclid(CHARACTER_LIST.len());
        // Load new title screen
        if let Ok(entity) = title_query.single() {
            commands.entity(entity).despawn_recursive();
        }

        let character = CHARACTER_LIST[game.character_selection];
        let file_path
            = Path::new("sprites")
                .join("title_cards")
                .join(format!("title_screen_{}.png", character)
        );
        let texture_handle: Handle<Texture> = asset_server.load(file_path);

        //Spawn title card
        commands.spawn()
            .insert_bundle(SpriteBundle {
                material: materials.add(texture_handle.into()),
                transform: Transform {
                    translation: Vec3::new(
                        (WINDOW_WIDTH / 2.0) * SCALE, 
                        (WINDOW_HEIGHT / 2.0) * SCALE, 
                        0.0
                    ),
                    scale: Vec3::new(SCALE, SCALE, SCALE),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Title);
    }
}

fn wait_for_start(
    mut state: ResMut<State<GameState>>,
    keyboard_input: Res<Input<KeyCode>>
) {
    if keyboard_input.just_pressed(KeyCode::R) {
        state.set(GameState::Start).unwrap();
    }
}

fn wait_for_play(
    mut state: ResMut<State<GameState>>,
    keyboard_input: Res<Input<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::Space) {
        state.set(GameState::Playing).unwrap();
    }
}

fn push_pause_state(
    mut state: ResMut<State<GameState>>,
    keyboard_input: Res<Input<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::P) || keyboard_input.just_pressed(KeyCode::Escape) {
        state.push(GameState::Pause).unwrap();
    }
}

fn pause_input(
    mut state: ResMut<State<GameState>>,
    keyboard_input: Res<Input<KeyCode>>,
) {
    if keyboard_input.just_pressed(KeyCode::R) {
        state.pop().unwrap();
    }
}

fn remove_play_score(mut commands: Commands, entities: Query<Entity, With<(NodeBundle, TextBundle)>>) {
    for entity in entities.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

// remove all entities that are not a camera
fn teardown(mut commands: Commands, entities: Query<Entity, Without<Camera>>) {
    for entity in entities.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

// Game Over State
fn display_gameover_message(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    scoreboard: ResMut<Scoreboard>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {

    let end_qoute = match scoreboard.score {
        s if s < 20 => "You suck lol",
        s if s < 50 => "u r sub",
        s if s == 69 => "Nice.",
        s if s < 100 => "still sub",
        s if s < 200 => "less sub",
        s if s == 420 => "HFFFFFFFFFFFFFFF",
        s if s == 666 => "u r evil",
        s if s < 1000 => "half sub",
        _ => "okay I guess u can be dom for tonight..."
    };

    commands
        .spawn_bundle(NodeBundle {
            style: Style {
                margin: Rect::all(Val::Auto),
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            material: materials.add(Color::NONE.into()),
            ..Default::default()
        })
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text::with_section(
                    format!("{}\nScore: {:?}\nPress <R> to restart", end_qoute, scoreboard.score),
                    TextStyle {
                        font: asset_server.load(Path::new("fonts").join("slkscr.ttf")),
                        font_size: 32.0,
                        color: Color::rgb(0.0, 0.0, 0.0),
                    },
                    TextAlignment {
                        horizontal: HorizontalAlign::Center,
                        vertical: VerticalAlign::Center,
                    },
                ),
                ..Default::default()
            });
        });
}

fn scoreboard_system(scoreboard: Res<Scoreboard>, mut query: Query<&mut Text>) {
    let mut text = query.single_mut().unwrap();
    text.sections[0].value = format!("Score: {}", scoreboard.score);
}

fn main() {
    App::build()
        .init_resource::<Game>()
        .init_resource::<TitleCardTextures>()
        .init_resource::<FloatyTextures>()
        .insert_resource(WindowDescriptor {
            title: "Flappy Groof".to_string(),
            width: WINDOW_WIDTH,
            height: WINDOW_HEIGHT,
            // mode: bevy::window::WindowMode::BorderlessFullscreen,
            scale_factor_override: Some(2.0),
            resizable: false,
            ..Default::default()
        })
        .insert_resource(ClearColor(Color::rgb(1.0, 1.0, 0.5)))
        .insert_resource(Scoreboard { score: 0 })
        .insert_resource(PipeSpawnTimer(Timer::from_seconds(2.0, true)))
        .insert_resource(FloatySpawnTimer(Timer::from_seconds(0.75, true)))
        .add_plugins(DefaultPlugins)
        .add_state(GameState::MainMenu)
        .add_startup_system(load_title_cards.system())
        .add_startup_system(load_floaties.system())
        .add_startup_system(setup_cameras.system())
        // MainMenu state
        .add_system_set(SystemSet::on_enter(GameState::MainMenu)
            .with_system(start_setup.system())
            .with_system(display_start_message.system())
        )
        .add_system_set(SystemSet::on_update(GameState::MainMenu)
            .with_system(wait_for_start.system())
            .with_system(pick_character.system())
        )
        .add_system_set(SystemSet::on_exit(GameState::MainMenu)
            .with_system(teardown.system())
        )
        // Start State
        .add_system_set(SystemSet::on_enter(GameState::Start)
            .with_system(play_setup.system())
        )
        .add_system_set(SystemSet::on_update(GameState::Start)
            .with_system(floaty_spawner.system())
            .with_system(move_floaties.system())
            .with_system(wait_for_play.system())
        )
        // Playing state
        .add_system_set(SystemSet::on_enter(GameState::Playing)
        )
        .add_system_set(SystemSet::on_update(GameState::Playing)
            .with_system(floaty_spawner.system())
            .with_system(move_floaties.system())
            .with_system(groof_movement.system())
            .with_system(groof_collision.system())
            .with_system(push_pause_state.system())
            .with_system(pipe_spawner.system())
            .with_system(pipe_mover.system())
            .with_system(scoreboard_system.system())
        )
        .add_system_set(SystemSet::on_inactive_update(GameState::Playing)
            .with_system(floaty_spawner.system())
            .with_system(move_floaties.system())
        )
        // .add_system_set(SystemSet::on_exit(GameState::Playing).with_system(teardown.system()))
        // GameOver state
        .add_system_set(SystemSet::on_update(GameState::Pause)
            .with_system(pause_input.system())
        )
        .add_system_set(SystemSet::on_enter(GameState::GameOver)
            .with_system(remove_play_score.system())
            .with_system(display_gameover_message.system())
        )
        .add_system_set(SystemSet::on_update(GameState::GameOver)
            .with_system(floaty_spawner.system())
            .with_system(move_floaties.system())
            .with_system(groof_game_over.system())
            .with_system(wait_for_start.system())
        )
        .add_system_set(SystemSet::on_exit(GameState::GameOver)
            .with_system(teardown.system())
        )
        // .add_system(groof_movement.system())
        .run();
}
