use bevy::prelude::*;
use rand::prelude::*;
use std::path::Path;

use crate::{
    // WINDOW_HEIGHT,
    WINDOW_WIDTH,
    SCALE,
};

use crate::collider::*;

pub struct Pipe {
    pub scorable: bool,
    pub speed: f32,
}

pub struct PipeSpawnTimer(pub Timer);

pub fn pipe_spawner(
    time: Res<Time>, 
    mut timer: ResMut<PipeSpawnTimer>, 
    mut commands: Commands,
    asset_server: Res<AssetServer>, 
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    let texture_handle: Handle<Texture> = asset_server.load(Path::new("sprites").join("pipe.png"));
    
    if timer.0.tick(time.delta()).just_finished() {
        let mut rng = rand::thread_rng();
        commands.spawn()
            .insert_bundle(SpriteBundle {
                material: materials.add(texture_handle.clone().into()),
                transform: Transform {
                    translation: Vec3::new(
                        (WINDOW_WIDTH * SCALE) + 15.0,
                        rng.gen_range(50.0..125.0), 
                        3.0
                    ),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Pipe { 
                scorable: true,
                speed: -50.0,
            })
            .insert(Hitboxes {
                collision_boxes: vec![
                    CollisionBox {
                        collider: Collider::Scorable,
                        offset: Vec3::new(15.0, 0.0, 0.0),
                        size: Vec2::new(30.0, 50.0),
                    },
                    CollisionBox {
                        collider: Collider::Pipe,
                        offset: Vec3::new(0.0, 70.0, 0.0),
                        size: Vec2::new(30.0, 70.0),
                    },
                    CollisionBox {
                        collider: Collider::Pipe,
                        offset: Vec3::new(0.0, -70.0, 0.0),
                        size: Vec2::new(30.0, 70.0),
                    },
                ]
            });
    }
}

pub fn pipe_mover(
    mut commands: Commands,
    time: Res<Time>, 
    mut pipe_query: Query<(Entity, &Pipe, &mut Transform)>
) {
    for (entity, pipe, mut transform) in pipe_query.iter_mut() {
        if transform.translation.x < -15.0 {
            commands.entity(entity).despawn_recursive();
        }
        let translation = &mut transform.translation;
        translation.x += time.delta_seconds()* pipe.speed;
    }
}