use bevy::prelude::*;

pub struct Title;

#[derive(Default)]
pub struct TitleCardTextures {
    pub handles: Vec<HandleUntyped>,
}

// Setup
pub fn load_title_cards(
    // mut commands: Commands,
    mut title_card_handles: ResMut<TitleCardTextures>,
    asset_server: Res<AssetServer>,
) {
    title_card_handles.handles = asset_server.load_folder("sprites/title_cards").unwrap();
}