#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum GameState {
    MainMenu,
    Start,
    Playing,
    Pause,
    GameOver,
}