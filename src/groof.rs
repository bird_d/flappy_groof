use std::path::Path;
use bevy::{
    prelude::*,
    sprite::collide_aabb::collide,
};

use crate::{
    gamestate::GameState, 
    pipe::Pipe,
    Scoreboard,
    SCALE,
    WINDOW_HEIGHT,
};
use crate::collider::*;

pub struct Groof {
    pub gravity: f32,
    pub max_speed: f32,
    pub velocity: Vec3,
}

pub fn groof_movement(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    time: Res<Time>,
    keyboard_input: Res<Input<KeyCode>>,
    mut groof_query: Query<(&mut Groof, &mut Transform)>,
) {
    // Query for a single groof with transform
    if let Ok((mut groof, mut transform)) = groof_query.single_mut() {
        let gravity = groof.gravity;
        let max_speed = groof.max_speed;
        let velocity = &mut groof.velocity;

        // Fall
        velocity.y -= time.delta_seconds() * gravity;

        // Make sure to not fall too fast
        if velocity.y < -max_speed {
            velocity.y = -max_speed;
        }

        // Flap
        if keyboard_input.just_pressed(KeyCode::Space) 
        || keyboard_input.just_pressed(KeyCode::A)
        || keyboard_input.just_pressed(KeyCode::E) 
        || keyboard_input.just_pressed(KeyCode::F)
        || keyboard_input.just_pressed(KeyCode::Q)
        || keyboard_input.just_pressed(KeyCode::X)
        || keyboard_input.just_pressed(KeyCode::Z)
        {
            velocity.y = max_speed;

            // Play flapping sound
            let sound = asset_server.load(Path::new("audio").join("flap.mp3"));
            audio.play(sound);
        }
        
        // Move the sprite according to velocity
        transform.translation.y += time.delta_seconds() * velocity.y;

        // Rotate sprite
        transform.rotation = Quat::from_rotation_z(velocity.y / 720.0);
        
    }
}

pub fn groof_collision(
    audio: Res<Audio>,
    asset_server: Res<AssetServer>,
    mut state: ResMut<State<GameState>>,
    mut scoreboard: ResMut<Scoreboard>,
    groof_query: Query<(&Groof, &Transform, &CollisionBox)>,
    mut collider_query: Query<(&mut Pipe, &Transform, &Hitboxes)>,
) {
    if let Ok((_groof, groof_transform, groof_collision_box)) = groof_query.single() {
        // Die if go too far vertically
        if groof_transform.translation.y < 0.0 || groof_transform.translation.y > (WINDOW_HEIGHT * SCALE) {
            // Play death sound
            let sound = asset_server.load(Path::new("audio").join("hurt.mp3"));
            audio.play(sound);

            // Go to game over state
            state.set(GameState::GameOver).unwrap();
        }
        
        // Check collision with every pipe against groof's
        let groof_size = groof_collision_box.size;
        for (mut pipe, transform, hitboxes) in collider_query.iter_mut() {
            for collision_box in hitboxes.collision_boxes.iter() {
                let collision = collide(
                    groof_transform.translation + groof_collision_box.offset,
                    groof_size,
                    transform.translation + collision_box.offset,
                    collision_box.size
                );
    
                if let Some(_collision) = collision {
                    // Die
                    if let Collider::Pipe = collision_box.collider {
                        // Play death sound
                        let sound = asset_server.load(Path::new("audio").join("hurt.mp3"));
                        audio.play(sound);
                        
                        // Go to game over state
                        state.set(GameState::GameOver).unwrap();
                        break;
                    }
                    // Score~!!
                    if let Collider::Scorable = collision_box.collider{
                        if pipe.scorable {
                            scoreboard.score += 1;
                            pipe.scorable = false;
                        }
                    }
                }
            }
        }
    }
}

pub fn groof_game_over(
    time: ResMut<Time>,
    mut groof_query: Query<&mut Transform, With<Groof>>,
) {
    if let Ok(mut groof_transform) = groof_query.single_mut() {
        groof_transform.rotate(Quat::from_rotation_z(-25.0 * time.delta_seconds()));
        groof_transform.translation.z = 4.0;
    }
}