use bevy::prelude::*;
use rand::prelude::*;
// use std::path::Path;

use crate::{
    SCALE,
    WINDOW_HEIGHT,
    WINDOW_WIDTH,
};

pub struct Floaty {
    pub speed: f32,
}

pub struct FloatySpawnTimer(pub Timer);

#[derive(Default)]
pub struct FloatyTextures {
    pub handles: Vec<HandleUntyped>,
}

// Setup
pub fn load_floaties(
    // mut commands: Commands,
    mut floaty_handles: ResMut<FloatyTextures>,
    asset_server: Res<AssetServer>,
) {
    floaty_handles.handles = asset_server.load_folder("sprites/floaties").unwrap();
}

pub fn floaty_spawner(
    time: Res<Time>,
    mut floaty_timer: ResMut<FloatySpawnTimer>,
    floaty_handles: Res<FloatyTextures>,
    mut commands: Commands,
    // asset_server: Res<AssetServer>, 
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    if floaty_timer.0.tick(time.delta()).just_finished() {
        let mut rng = rand::thread_rng();

        let texture_handle = floaty_handles.handles[rng.gen_range(0..floaty_handles.handles.len())].clone_weak().typed::<Texture>();

        //Spawn groof
        commands.spawn()
            .insert_bundle(SpriteBundle {
                material: materials.add(texture_handle.into()),
                transform: Transform {
                    translation: Vec3::new(
                        (WINDOW_WIDTH * SCALE) + 32.0,
                        rng.gen_range(WINDOW_HEIGHT*0.2..WINDOW_HEIGHT*0.8) * SCALE, 
                        1.0
                    ),
                    rotation: Quat::from_rotation_z(rng.gen_range(0.0..360.0)),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Floaty {
                speed: -rng.gen_range(10.0..100.0)
            });
    }
}

pub fn move_floaties(
    mut commands: Commands,
    time: Res<Time>,
    mut floaties: Query<(Entity, &Floaty, &mut Transform)>
) {
    for (entity, floaty, mut transform) in floaties.iter_mut() {
        if transform.translation.x < -30.0 {
            commands.entity(entity).despawn_recursive();
        }
        transform.translation.x += time.delta_seconds() * floaty.speed;
    }
}