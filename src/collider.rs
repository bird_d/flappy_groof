use bevy::math::{
    Vec2,
    Vec3,
};

pub enum Collider {
    Groof,
    Pipe,
    Scorable,
}

pub struct CollisionBox {
    pub collider: Collider,
    pub offset: Vec3,
    pub size: Vec2,
}

pub struct Hitboxes {
    pub collision_boxes: Vec<CollisionBox>
}